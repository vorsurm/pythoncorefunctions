import csv
import math
import json
import time
import tweepy
from tweepy import Stream
from tweepy import OAuthHandler
from tweepy.streaming import StreamListener

with open("credentials.json", "r") as file:
	cred_data = json.load(file)
	creds = cred_data['twitter']

auth = OAuthHandler(creds['consumer_key'], creds['consumer_secret'])
auth.set_access_token(creds['access_token'], creds['access_secret'])

api = tweepy.API(auth)

input_users = input("Enter the number of collected users: ")

inlcude_tags = []
input_location = input("Enter user location: ")
inlcude_tags.append(input_location)
include_tag_number = input("Enter number of include tags you want to enter: ")
for x in range(int(include_tag_number)):
	input_tag = input("Enter include tag {}: ".format(x+1))
	inlcude_tags.append(input_tag)

exlcude_tags = []
exclude_tag_number = input("Enter number of exclude tags you want to enter: ")
for x in range(int(exclude_tag_number)):
	input_tag = input("Enter exclude tag {}: ".format(x+1))
	exlcude_tags.append(input_tag)

input_status_count = input("Enter number of minimum statuses: ")

total_saved = 0
input_users = int(input_users)

current_users = open('users.csv', 'r').read()
writer = csv.writer(open("users_sample.csv", 'a', newline=''))

if input_users < 20 :
	for user in api.search_users(q=inlcude_tags, count= input_users):
		if any(word in user.description for word in exlcude_tags) or user.screen_name in current_users:
			pass
		elif user.statuses_count >= int(input_status_count):
			writer.writerow([user.screen_name])
			total_saved = total_saved+1
else:
	count = 20
	pages = math.ceil(input_users/20)
	i = 1
	while i <= pages:
		for user in api.search_users(q=inlcude_tags, count= count, page= i):
			if any(word in user.description for word in exlcude_tags) or user.screen_name in current_users or total_saved >= input_users:
				pass
			elif user.statuses_count >= int(input_status_count):
				writer.writerow([user.screen_name])
				total_saved = total_saved+1
		i += 1

print('Total new user colleted: {}'.format(total_saved))